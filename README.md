# quick-start

快速启动

## 遍历jar目录下的zipkin-server-xxx-exec.jar,选择对应序号的jar启动

*start-jar.sh*

```shell
#!/bin/bash
java -jar quick-start-0.0.1.jar

```

*start-jar-8081.sh*

```shell
#!/bin/bash
java -jar quick-start-0.0.1.jar --port=8081

```